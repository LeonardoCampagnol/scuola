/**
 * Classe che tiene conto del tempo trascorso
 * @author Leonardo Campagnol
 * @date 11/12/2018
 * @version 1.1
 */

public class Orario extends Thread{

    public int tempo;
    public int tempoMassimo;

    public Orario(int tempoMassimo){

        this.tempo = 0;
        this.tempoMassimo = tempoMassimo;
    }

    @Override
    public void run() {

        for (int i = 0; i < this.tempoMassimo; i++) {

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            tempo += 1;
        }
    }
}
