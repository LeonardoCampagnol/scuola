/**
 * Classe che riempie l'array circolare
 * @author Leonardo Campagnol
 * @date 11/12/2018
 * @version 1.1
 */

import java.util.Random;

public class MacchineEntranti implements Runnable{

    private Buffer<String> buffer;
    private int max;
    private Orario orario;

    /**
     * Costruttore del runnable MacchineEntranti
     * @param buffer buffer circolare
     * @param max numero massimo di elementi che entreranno nel buffer
     * @param orario tempo generale del programma
     */
    public MacchineEntranti(Buffer<String> buffer,int max,Orario orario){

        this.buffer = buffer;
        this.max = max;
        this.orario = orario;

    }

    @Override
    public void run() {

        int i = 0;

        try{

            while(i < max) {

                Thread.sleep(1000);
                System.out.println("Sono la macchina "+i+", ed entro al secondo "+orario.tempo+", nella posizione "+buffer.in);
                this.buffer.add(""+i++);

            }

        }catch(InterruptedException e) {
            e.printStackTrace();
        }
    }
}
