/**
 * Arrai circolare
 * @author Leonardo Campagnol
 * @date 11/12/2018
 * @version 1.1
 */

import java.util.concurrent.Semaphore;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

class Buffer<T> {
    private Object[] buffer;
    public Semaphore oggettiPrendibili;
    public Semaphore spaziDisponibili;
    private int dim;
    private Lock mutex;
    public int in;
    public int out;

    public Buffer(int dim) {

        this.dim = dim;
        this.buffer = new Object[dim];
        this.oggettiPrendibili = new Semaphore(0);
        this.spaziDisponibili = new Semaphore(dim);
        this.mutex = new ReentrantLock();
        this.in = 0;
        this.out = 0;

    }

    public void add(T oggetto) {

        try{

            this.spaziDisponibili.acquire();
            synchronized (this) {
                this.buffer[in] = oggetto;
                in = (in+1) % dim;
                this.oggettiPrendibili.release();

            }

        }
        catch(InterruptedException e) {
            e.printStackTrace();
        }

    }

    public T getAndRemove() {

        T ret = null;

        try{

            this.oggettiPrendibili.acquire();
            synchronized (this) {
                ret = (T)this.buffer[out];
                this.buffer[out] = null;
                out = (out+1) % dim;
                this.spaziDisponibili.release();

            }

        }
        catch(InterruptedException e) {
            e.printStackTrace();
        }

        return ret;

    }

    public void scriviTutto() {

        System.out.print("{");

        for(Object t: buffer) {

            if(t != null) {
                System.out.print(t+";");
            } else {
                System.out.print(";");
            }

        }

        System.out.println("}");

    }

}
