Leonardo Campagnol

Il progetto � suddiviso in 5 classi.

Parcheggio: ecquivale alla classe Main, al suo interno vengono dichiarati
rispettivamente un buffer circolare, un orario ed i due thred.

Orologio: � una classe che estende thread ed ha il compito di tenere conto
del tempo che passa dall'inizio dell'esecuzione. Nel suo costruttore
� necessario inserire come parametro il tempo che dovr� raggiungere prima
di fermarsi, che in questo caso � impostato a 20s da Parcheggio.

Buffer: come dice il nome si tratta di una sorta wrapper class per un array
che per� viene reso circolare ed al quale vengono implementate le funzioni
per aggiungere o togliere un elemento da esso. Per inizializzarne uno �
necessario passare al costruttore la sua grandezza. Esso funziona attraverso
l'utilizzo di due semafori che specificano gli sapzzi liberi e quelli
occupati all'interno dell'array, e la sezione critica � messa in atto dal
syncronized.

MacchineEntranti: classe che estende thread, a cui vengono passati un buffer
il numero massimo di elementi che dovranno essere messi in esso durante
l'interea esecuzione del programma ed un orario. Il compito di questa
classe � infatti quello di aggiungere un elemento all'array circolare,
dicendo il nome dell'elemento aggiunto, a che secondo � stato inserito e
a che posizione.

MacchineUscenti: classe che estende thread, a cui vengono passati un buffer
ed un orario. Il compto di questa classe � infatti quello di togliere gli
elementi dall'array circolare. Per farlo essa dichiara a sua volta dei
thread, tramite un'espressione lambda, che, dopo avre aspettato il giusto
ammontare di secondi, svuoteranno il buffer di un elemento. Inoltre questi
thread specificheranno che elemento � stato tolto, a quale secondo e quanto
ha sostato. Una volta trascorso il tempo massimo dell'orario, la classe
comunicher� su console il guadagno totale.