/**
 * @author Leonardo Campagnol
 * @date 11/12/2018
 * @version 1.1
 */

public class Parcheggio {
    public static void main(String[] args) throws InterruptedException {

        Buffer<String> posti = new Buffer<>(6);

        System.out.println("Il parcheggio è aperto!");
        Orario orario = new Orario(20);
        orario.start();

        Thread t1 = new Thread(new MacchineEntranti(posti,10,orario));
        t1.start();
        Thread t2 = new Thread(new MacchineUscenti(posti,orario));
        t2.start();

        while(orario.tempo<orario.tempoMassimo) {
            //posti.scriviTutto();
            Thread.sleep(1000);
        }

        System.out.println("Il parcheggio è chiuso!");
        System.out.println("Il parcheggio ha guadagnato: ");

    }
}
