/**
 * Classe che svuota l'array circolare
 * @author Leonardo Campagnol
 * @date 11/12/2018
 * @version 1.1
 */

import java.util.Random;

public class MacchineUscenti implements Runnable{

    private Buffer<String> buffer;
    private Orario orario;
    private int guadagno;
    private Random r;

    /**
     * Costruttore del runnable MacchineUscenti
     * @param buffer buffer circolare
     * @param orario tempo generale del programma
     */
    public MacchineUscenti(Buffer<String> buffer,Orario orario) {

        this.buffer = buffer;
        this.orario = orario;
        this.guadagno = 0;
        this.r = new Random();

    }
    @Override
    public void run() {

        try{

            while(this.orario.tempo<this.orario.tempoMassimo) {

                Thread.sleep(1010);
                int tempoDiSosta = r.nextInt(4) + 2;

                new Thread(()->{
                    try {
                        Thread.sleep(tempoDiSosta*1000);
                        System.out.println("Sono la macchina "+this.buffer.getAndRemove()+", ed esco al secondo "+orario.tempo+", ed ho pagato "+tempoDiSosta+"euro");
                        guadagno+=tempoDiSosta;
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }).start();
            }

            Thread.sleep(2000);
            System.out.println(guadagno+"euro");

        }catch(InterruptedException e){
            e.printStackTrace();
        }
    }
}
